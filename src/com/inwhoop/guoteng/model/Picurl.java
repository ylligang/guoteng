package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**  
 * @Project: GuoTeng
 * @Title: Picurl.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-25 上午10:00:56
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class Picurl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String title;
	public String imgurl;

}

/**
 * 
 */
package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**
 * @author Administrator
 * 
 * 咨询信息顶部推荐信息
 *
 */
public class Infonewspic implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public int id;
	public String title;
	public String imgpath;

}

package com.inwhoop.guoteng.model;

import java.io.Serializable;

import com.inwhoop.guoteng.util.Parameter;

/**  
 * @Project: GuoTeng
 * @Title: PrivateInfo.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO 私人定制信息
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-21 下午2:22:55
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class PrivateInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String CustomerName;
	public String UserName;
	public String Tel;
	public String Explain;

}

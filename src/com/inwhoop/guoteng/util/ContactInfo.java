package com.inwhoop.guoteng.util;

import java.io.Serializable;

/**  
 * @Project: GuoTeng
 * @Title: ContactInfo.java
 * @Package com.inwhoop.guoteng.util
 * @Description: TODO 联系我们的详情
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-20 下午2:31:21
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class ContactInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String Webtel;
	public String WebSaleTel;
	public String Webfax;
	public String Webmail;
	public String WebQQ;
	public String WebAddress;
	public String WebCode;
	public String Weburl;

}

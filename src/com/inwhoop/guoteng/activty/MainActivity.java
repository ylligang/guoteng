package com.inwhoop.guoteng.activty;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.HomepagePictureInfo;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

/**
 * 主界面
 * 
 * @Project: GuoTeng
 * @Title: MainActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-3-10 上午2:58:42
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class MainActivity extends BaseActivity implements OnClickListener {

	private ViewPager viewPager = null;

	private List<View> imgList = new ArrayList<View>();

	private List<View> pointList = new ArrayList<View>();

	private RelativeLayout imgLayout;

	private LinearLayout pointLayout; // 点点

	private List<HomepagePictureInfo> piclist = new ArrayList<HomepagePictureInfo>();

	private LayoutInflater inflater = null;

	private LinearLayout zeroLayout, oneLayout, twoLayout, productLayout,
			infoLayout, aboutLayout, privateLayout, serviceLayout, promoLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MyApplication.sw = getWindowManager().getDefaultDisplay().getWidth();
		MyApplication.sh = getWindowManager().getDefaultDisplay().getHeight();
		mContext = MainActivity.this;
		inflater = LayoutInflater.from(mContext);
		initData();
	}

	/**
	 * 数据初始化
	 * 
	 * @Title: initData
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void initData() {
		super.init();
		setTitleName(R.string.app_name);
		setLeftButton(R.drawable.left_bt);
		viewPager = (ViewPager) findViewById(R.id.showimg_pager);
		imgLayout = (RelativeLayout) findViewById(R.id.img_rela);
		LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, MyApplication.sh / 11 * 2-5);
		parm.setMargins(5, 5, 5, 0);
		pointLayout = (LinearLayout) findViewById(R.id.point_lin);
		zeroLayout = (LinearLayout) findViewById(R.id.zerolayout);
		zeroLayout.setLayoutParams(parm);
		oneLayout = (LinearLayout) findViewById(R.id.onelayout);
		oneLayout.setLayoutParams(parm);
		twoLayout = (LinearLayout) findViewById(R.id.twolayout);
		LinearLayout.LayoutParams parm1 = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, MyApplication.sh / 11 * 2-5);
		parm1.setMargins(5, 5, 5, 5);
		twoLayout.setLayoutParams(parm1);
		LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(
				MyApplication.sw / 5 * 3, LayoutParams.FILL_PARENT);
		parms.setMargins(0, 0, 5, 0);
		productLayout = (LinearLayout) findViewById(R.id.pruductlayout);
		productLayout.setOnClickListener(this);
		productLayout.setLayoutParams(parms);
		infoLayout = (LinearLayout) findViewById(R.id.infolayout);
		infoLayout.setOnClickListener(this);
		aboutLayout = (LinearLayout) findViewById(R.id.aboutlayout);
		aboutLayout.setOnClickListener(this);
		privateLayout = (LinearLayout) findViewById(R.id.privatelayout);
		privateLayout.setOnClickListener(this);
		serviceLayout = (LinearLayout) findViewById(R.id.servicelayout);
		serviceLayout.setOnClickListener(this);
		promoLayout = (LinearLayout) findViewById(R.id.promolayout);
		promoLayout.setOnClickListener(this);
		readPicinfo();

	}

	private void readPicinfo() {
		showProgressDialog("正在加载信息...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					piclist = getPicinfo();
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("数据加载失败，请检查您的网络~~");
				break;

			case MyApplication.READ_SUCCESS:
				setViewPager();
				break;

			default:
				break;
			}
		};
	};

	/**
	 * 
	 * @Title: viewpagerInit
	 * @Description: TODO viewpager的初始化
	 * @param
	 * @return void
	 */
	private void setViewPager() {
		if (null != piclist && piclist.size() > 0) {
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, (MyApplication.sh) / 12 * 4 - 15);
			imgLayout.setLayoutParams(parm);
			for (int i = 0; i < piclist.size(); i++) {
				View v = inflater.inflate(R.layout.gt_viewpager_item, null);
				ImageView img = (ImageView) v.findViewById(R.id.v_img);
				img.setLayoutParams(parm);
				img.setScaleType(ScaleType.CENTER_CROP);
				// fb.display(img, piclist.get(i));
				img.setBackgroundResource(R.drawable.h1);
				imgList.add(v);
				ImageView point = new ImageView(this);
				point.setBackgroundResource(R.drawable.gray1);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				params.setMargins(5, 0, 5, 0);
				point.setLayoutParams(params);
				pointList.add(point);
				pointLayout.addView(point);
			}
			pointList.get(0).setBackgroundResource(R.drawable.green1);
			viewPager.setAdapter(new MyPageAdapter());
			viewPager.setOnPageChangeListener(new MyPagelistener());
		}
	}

	class MyPagelistener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int position) {
			for (int i = 0; i < pointList.size(); i++) {
				pointList.get(i).setBackgroundResource(R.drawable.gray1);
				if (i == position) {
					pointList.get(i).setBackgroundResource(R.drawable.green1);
				}
			}
		}

	}

	class MyPageAdapter extends PagerAdapter {

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(imgList.get(arg1));
		}

		@Override
		public int getCount() {
			return imgList.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return (arg0 == arg1);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			((ViewPager) container).addView(imgList.get(position));
			return imgList.get(position);
		}

	}

	/**
	 * 测试数据
	 */
	private List<HomepagePictureInfo> getPicinfo() {
		for (int i = 0; i < 5; i++) {
			HomepagePictureInfo info = new HomepagePictureInfo();
			info.id = i + 1;
			info.imgpath = "";
			piclist.add(info);
		}
		return piclist;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.aboutlayout:
			
			break;
		case R.id.pruductlayout:

			break;

		case R.id.infolayout:

			break;

		case R.id.privatelayout:

			break;

		case R.id.servicelayout:

			break;

		case R.id.promolayout:

			break;

		default:
			break;
		}
	}

}

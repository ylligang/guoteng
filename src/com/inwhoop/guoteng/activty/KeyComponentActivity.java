package com.inwhoop.guoteng.activty;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.BarsetInfo;
import com.inwhoop.guoteng.model.CommentKinds;
import com.inwhoop.guoteng.util.JsonUtils;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * @Project: GuoTeng
 * @Title: KeyComponentActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO 关键元器件
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-3-14 上午11:00:03
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class KeyComponentActivity extends BaseActivity {

	private LinearLayout itemLayout = null;

	private LayoutInflater inflater = null;

	private LinearLayout guideLayout = null;

	private ScrollView scrollView = null;

	private BarsetInfo info = null;

	private TextView introTextView = null; // 简介

	private List<CommentKinds> list = null;

	private TextView failTextView = null;
	
	private int count = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.key_component_layout);
		mContext = KeyComponentActivity.this;
		inflater = LayoutInflater.from(mContext);
		info = (BarsetInfo) getIntent().getSerializableExtra("info");
		init();
	}

	@Override
	protected void init() {
		super.init();
		setTitleName("关键元器件");
		setLeftButton(R.drawable.back);
		itemLayout = (LinearLayout) findViewById(R.id.itemlayout);
		guideLayout = (LinearLayout) findViewById(R.id.guidelayout);
		scrollView = (ScrollView) findViewById(R.id.scroll);
		introTextView = (TextView) findViewById(R.id.introtext);
		failTextView = (TextView) findViewById(R.id.failtext);
		if (null != info)
			introTextView.setText(Html.fromHtml(info.content));
		read();
	}

	private void read() {
		showProgressDialog("正在加载信息...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getCommentKinds();
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				failTextView.setVisibility(View.VISIBLE);
				scrollView.setVisibility(View.GONE);
				break;

			case MyApplication.READ_SUCCESS:
				failTextView.setVisibility(View.GONE);
				scrollView.setVisibility(View.VISIBLE);
				setGuidelayout();
				break;

			default:
				break;
			}
		};
	};

	private void setGuidelayout() {
		if (null != list && list.size() > 0) {

			final List<View> lines = new ArrayList<View>();
			final List<TextView> texts = new ArrayList<TextView>();
			for (int i = 0; i < list.size(); i++) {
				View view = inflater.inflate(R.layout.product_show_item, null);
				LinearLayout layout = (LinearLayout) view
						.findViewById(R.id.onelayout);
				layout.setTag(i + 1);
				AbsListView.LayoutParams parm = new AbsListView.LayoutParams(
						MyApplication.sw / 9*2, LayoutParams.WRAP_CONTENT);
				layout.setLayoutParams(parm);
				TextView nameTextView = (TextView) view.findViewById(R.id.name);
				nameTextView.setText(list.get(i).title);
				View line = view.findViewById(R.id.line);
				lines.add(line);
				ImageView sline = (ImageView) view.findViewById(R.id.sline);
				if(i==list.size()-1){
					sline.setVisibility(View.GONE);
				}
				texts.add(nameTextView);
				layout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						int position = (Integer) view.getTag();
						for (int j = 0; j < lines.size(); j++) {
							lines.get(j).setVisibility(View.GONE);
							texts.get(j).setTextColor(
									getResources().getColor(R.color.black));
						}
						lines.get(position - 1).setVisibility(View.VISIBLE);
						texts.get(position - 1).setTextColor(
								getResources().getColor(
										R.color.guide_text_choice));
						swithActivity(position - 1,list.get(position-1).category_id);
					}
				});
				guideLayout.addView(view);
			}
			for (int j = 0; j < lines.size(); j++) {
				lines.get(j).setVisibility(View.GONE);
			}
			lines.get(0).setVisibility(View.VISIBLE);
			texts.get(0).setTextColor(
					getResources().getColor(R.color.guide_text_choice));
			swithActivity(0,list.get(0).category_id);
		}
	}

	/**
	 * activity跳转
	 * 
	 * @Title: swithActivity
	 * @Description: TODO
	 * @param @param position
	 * @return void
	 */
	private void swithActivity(int position,int categoryid) {
		itemLayout.removeAllViews();
		Intent intent = new Intent(this, ActProductShow.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.putExtra("categoryid", categoryid);
		intent.putExtra("count", count);
		String name = position + " subactivity";
		View subActivity = getLocalActivityManager()
				.startActivity(name, intent).getDecorView();
		itemLayout.addView(subActivity,
				RelativeLayout.LayoutParams.FILL_PARENT,
				RelativeLayout.LayoutParams.FILL_PARENT);
		scrollView.smoothScrollBy(0, -1);
		count ++;
	}

}

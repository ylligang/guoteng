/**
 * 
 */
package com.inwhoop.guoteng.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * 解决方案的详细内容
 */
public class SolutionInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String content;
	public String title;
	public List<Picurl> list = new ArrayList<Picurl>();
}

package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**  
 * @Project: GuoTeng
 * @Title: Feedback.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO 
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-21 下午2:04:12
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class Feedback implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String content;
	public int categoryId;
	public String alias;
	
}

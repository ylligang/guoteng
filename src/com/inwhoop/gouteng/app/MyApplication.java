package com.inwhoop.gouteng.app;

import android.app.Application;

/**  
 * @Project: GuoTeng
 * @Title: MyApplication.java
 * @Package com.inwhoop.gouteng.app
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-12 下午4:07:45
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class MyApplication extends Application{

	public static int sw;
	public static int sh;
	
	public final static int READ_SUCCESS = 200;  //读取网络数据成功
	public final static int READ_FAIL = 500;  //读取网络失败
	
}

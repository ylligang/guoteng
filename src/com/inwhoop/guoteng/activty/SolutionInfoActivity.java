/**
 * 
 */
package com.inwhoop.guoteng.activty;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.activty.MainActivity.MyPageAdapter;
import com.inwhoop.guoteng.activty.MainActivity.MyPagelistener;
import com.inwhoop.guoteng.model.BarsetInfo;
import com.inwhoop.guoteng.model.CommentKinds;
import com.inwhoop.guoteng.model.SolutionInfo;
import com.inwhoop.guoteng.model.SolutionList;
import com.inwhoop.guoteng.util.JsonUtils;
import com.inwhoop.guoteng.view.MyScrollview;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;

/**
 * @author Administrator
 *  解决方案的详细内容
 */
public class SolutionInfoActivity extends BaseActivity implements OnClickListener{
	
	private ViewPager viewPager = null;

	private List<View> imgList = new ArrayList<View>();

	private RelativeLayout imgLayout;

	private LayoutInflater inflater = null;
	
	private TextView titlenameTextView, nowpositionTextView,nameTextView,contentTextView;
	
	private int nowposition = 1;  //图片滑动的位置
	
	private List<SolutionInfo> list = new ArrayList<SolutionInfo>();
	
	private SolutionList info = null;
	
	private MyScrollview scrollView = null;

	private TextView failTextView = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.solution_info_layout);
		mContext = SolutionInfoActivity.this;
		info = (SolutionList) getIntent().getSerializableExtra("info");
		inflater = LayoutInflater.from(mContext);
		init();
	}
	
	@Override
	protected void init() {
		super.init();
		setTitleName("解决方案");
		setLeftButton(R.drawable.back);
		imgLayout = (RelativeLayout) findViewById(R.id.img_rela);
		LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, (MyApplication.sh) / 12 * 4 - 15);
		imgLayout.setLayoutParams(parm);
		viewPager = (ViewPager) findViewById(R.id.showimg_pager);
		titlenameTextView = (TextView) findViewById(R.id.titlename);
		nowpositionTextView = (TextView) findViewById(R.id.nowposition);
		nameTextView = (TextView) findViewById(R.id.title);
		nameTextView.setText(info.title);
		contentTextView = (TextView) findViewById(R.id.content);
		scrollView = (MyScrollview) findViewById(R.id.scroll);
		scrollView.setVisibility(View.GONE);
		failTextView = (TextView) findViewById(R.id.failtext);
		readPicinfo();
	}

	private void readPicinfo() {
		showProgressDialog("正在加载信息...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getSolutionInfolist(info.article_id);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				failTextView.setVisibility(View.VISIBLE);
				scrollView.setVisibility(View.GONE);
				break;

			case MyApplication.READ_SUCCESS:
				failTextView.setVisibility(View.GONE);
				scrollView.setVisibility(View.VISIBLE);
				setViewPager();
				break;

			default:
				break;
			}
		};
	};
	
	/**
	 * 
	 * @Title: viewpagerInit
	 * @Description: TODO viewpager的初始化
	 * @param
	 * @return void
	 */
	private void setViewPager() {
		if (null != list && list.size() > 0) {
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, (MyApplication.sh) / 12 * 4 - 15);
			for (int i = 0; i < list.get(0).list.size(); i++) {
				View v = inflater.inflate(R.layout.gt_viewpager_item, null);
				ImageView img = (ImageView) v.findViewById(R.id.v_img);
				img.setLayoutParams(parm);
				img.setScaleType(ScaleType.CENTER_CROP);
				fb.display(img, MyApplication.HOST+list.get(0).list.get(i).imgurl);
				imgList.add(v);
			}
			viewPager.setAdapter(new MyPageAdapter());
			viewPager.setOnPageChangeListener(new MyPagelistener());
			nowpositionTextView.setText("1/" + list.get(0).list.size());
			titlenameTextView.setText(list.get(0).list.get(0).title);
			contentTextView.setText(list.get(0).content);
		}
	}
	
	class MyPagelistener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int position) {
			viewPager.setCurrentItem(position);
			titlenameTextView.setText(list.get(0).list.get(position).title);
			nowpositionTextView.setText((position+1)+"/"+list.get(0).list.size());
		}

	}
	
	class MyPageAdapter extends PagerAdapter {

		@Override
		public void destroyItem(View arg0, int position, Object arg2) {
			((ViewPager) arg0).removeView(imgList.get(position));
		}

		@Override
		public int getCount() {
			return imgList.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return (arg0 == arg1);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			((ViewPager) container).addView(imgList.get(position));
			return imgList.get(position);
		}

	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		default:
			break;
		}
	}
	

}

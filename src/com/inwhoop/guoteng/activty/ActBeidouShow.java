package com.inwhoop.guoteng.activty;

import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalBitmap;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.activty.ActProductShow.MyAdapter;
import com.inwhoop.guoteng.model.ProductInfo;
import com.inwhoop.guoteng.util.JsonUtils;
import com.inwhoop.guoteng.view.MyGridView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * @Project: GuoTeng
 * @Title: ProductShowActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO 产品展示
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-3-18 上午11:12:11
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ActBeidouShow extends BaseActivity {

	private MyGridView gridview = null;

	private List<ProductInfo> list = new ArrayList<ProductInfo>();

	private LayoutInflater inflater = null;

	private int categoryid;

	public FinalBitmap fb;

	private TextView nodata = null;
	
	private int count = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_product_layout);
		mContext = ActBeidouShow.this;
		inflater = LayoutInflater.from(mContext);
		categoryid = getIntent().getIntExtra("categoryid", 0);
		count = getIntent().getIntExtra("count", 0);
		fb = FinalBitmap.create(mContext);
		fb.configLoadingImage(R.drawable.default_local);
		fb.configLoadfailImage(R.drawable.default_local);
		findData();
	}

	private void findData() {
		gridview = (MyGridView) findViewById(R.id.progridview);
		nodata = (TextView) findViewById(R.id.nodata);
		read();
	}

	private void read() {
		if(count != 1){
			showProgressDialog("正在加载信息...");
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getBDZDProductList(categoryid);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				gridview.setVisibility(View.GONE);
				nodata.setVisibility(View.VISIBLE);
				break;

			case MyApplication.READ_SUCCESS:
				if (list.size() > 0) {
					gridview.setVisibility(View.VISIBLE);
					nodata.setVisibility(View.GONE);
					gridview.setAdapter(new MyAdapter());
					gridview.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							Intent intent = new Intent(mContext,
									BeidouTerminalInfoActivity.class);
							Bundle bundle = new Bundle();
							bundle.putSerializable("info", list.get(position));
							intent.putExtras(bundle);
							startActivity(intent);
						}
					});
				} else {
					gridview.setVisibility(View.GONE);
					nodata.setVisibility(View.VISIBLE);
				}
				break;

			default:
				break;
			}
		};
	};

	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressWarnings("unused")
		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			Holder holder = null;
			if (null == holder) {
				holder = new Holder();
				convertView = inflater.inflate(
						R.layout.progridview_item_layout, null);
				holder.layout = (RelativeLayout) convertView
						.findViewById(R.id.itemrlea);
				holder.img = (ImageView) convertView.findViewById(R.id.img);
				LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
						android.widget.LinearLayout.LayoutParams.FILL_PARENT,
						MyApplication.sw / 3 - 15);
				holder.layout.setLayoutParams(parm);
				holder.version = (TextView) convertView
						.findViewById(R.id.pname);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			holder.version.setText(list.get(position).title);
			fb.display(holder.img, MyApplication.HOST
					+ list.get(position).img_url);
			return convertView;
		}

		class Holder {
			ImageView img;
			TextView version;
			RelativeLayout layout;
		}
	}
}

package com.inwhoop.guoteng.activty;

import java.util.Set;

import cn.jpush.android.api.InstrumentedActivity;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.recever.MyReceiver;
import com.inwhoop.guoteng.util.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;

/**
 * @Project: GuoTeng
 * @Title: StartActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO 开始页面
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-3-20 下午4:34:20
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class StartActivity extends InstrumentedActivity {

	private MyReceiver myReceiver = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.start_layout);
		handler.sendEmptyMessageDelayed(0, 2000);
		JPushInterface.setDebugMode(true);
		JPushInterface.init(this);
		JPushInterface.setAliasAndTags(getApplicationContext(),
				"" + Utils.getDEVICEID(StartActivity.this), null,
				new TagAliasCallback() {

					@Override
					public void gotResult(int arg0, String arg1,
							Set<String> arg2) {

					}
				});
		// registerMessageReceiver();
	}

	public void registerMessageReceiver() {
		myReceiver = new MyReceiver();
		IntentFilter filter = new IntentFilter();
		filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
		filter.addAction(MyApplication.MESSAGE_RECEIVED_ACTION);
		registerReceiver(myReceiver, filter);
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			Intent intent = new Intent(StartActivity.this, MainActivity.class);
			startActivity(intent);
			finish();
		};
	};

}

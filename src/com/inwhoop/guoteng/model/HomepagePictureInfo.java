package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**  
 * @Project: GuoTeng
 * @Title: HomepagePictureInfo.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-13 下午5:46:59
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class HomepagePictureInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public String imgpath;

}

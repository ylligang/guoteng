package com.inwhoop.guoteng.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.text.format.Time;

public class Utils {

	/**
	 * 保存数据到sp
	 * 
	 * @Title: savePreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @param value 值
	 * @return void
	 */
	public static void savePreference(Context context, String key, String value) {
		PreferenceManager.getDefaultSharedPreferences(context).edit()
				.putString(key, value).commit();
	}

	/**
	 * 从sp获取数据
	 * 
	 * @Title: getpreference
	 * @Description: TODO
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @return
	 * @return String 返回的值
	 */
	public static String getpreference(Context context, String key) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(key, "");
	}


	/**
	 * 判断email格式是否正确
	 * 
	 * @Title: isEmail
	 * @Description: TODO
	 * @param @param email
	 * @param @return
	 * @return boolean
	 */
	public static boolean isEmail(String email) {
		String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(email);

		return m.matches();
	}

	/**
	 * 验证手机号
	 * 
	 * @Title: isMobileNO
	 * @Description: TODO
	 * @param @param mobiles
	 * @param @return
	 * @return boolean
	 * @throws
	 */
	public static boolean isMobileNO(String mobiles) {
		Pattern p = Pattern
				.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	/**
	 * 用来判断服务是否运行.
	 * 
	 * @param context
	 * @param className
	 *            判断的服务名字
	 * @return true 在运行 false 不在运行
	 */
	public static boolean isServiceRunning(Context mContext, String className) {
		boolean isRunning = false;
		ActivityManager activityManager = (ActivityManager) mContext
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> serviceList = activityManager
				.getRunningServices(30);
		if (!(serviceList.size() > 0)) {
			return false;
		}
		for (int i = 0; i < serviceList.size(); i++) {
			if (serviceList.get(i).service.getClassName().equals(className) == true) {
				isRunning = true;
				break;
			}
		}
		return isRunning;
	}

	/**
	 * 
	 * @Title: getDate
	 * @Description: TODO 裁剪时间字符串
	 * @param @param createdate
	 * @param @return
	 * @return String
	 */
	public static String getDate(String createdate) {
		if (!"".equals(createdate) && createdate.length() > 11) {
			if (createdate.indexOf("T") != -1) {
				return createdate.substring(0, createdate.indexOf("T"));
			}
		}
		return "";
	}

	/**
	 * 
	 * @Title: getDate
	 * @Description: TODO 裁剪时间字符串
	 * @param @param createdate
	 * @param @return
	 * @return String
	 */
	public static String getallDate(String createdate) {
		if (!"".equals(createdate) && createdate.length() > 11) {
			if (createdate.indexOf("T") != -1) {
				return createdate.substring(0, createdate.indexOf("T"))+" "
						+ createdate.substring(createdate.indexOf("T") + 1,
								createdate.lastIndexOf(":"));
			}
		}
		return "";
	}

	public static int getWeek(String createdate) {
		if (!"".equals(createdate) && createdate.length() > 11) {
			if (createdate.indexOf("T") != -1) {
				String sdate = createdate.substring(0, createdate.indexOf("T"));
				Date date = Date.valueOf(sdate);
				Calendar c = Calendar.getInstance();
				c.setTime(date);
				return c.get(Calendar.DAY_OF_WEEK);
			}
		}
		return -1;
	}

	@SuppressLint("SimpleDateFormat")
	public static String getTime(String createdate) throws ParseException {
		if (!"".equals(createdate) && createdate.length() > 11) {
			if (createdate.indexOf("T") != -1) {
				String sdate = createdate.replace("T", " ");
				SimpleDateFormat dfd = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat df = new SimpleDateFormat("HH:mm");
				// String d = dfd.format(sdate);
				return df.format(dfd.parse(sdate));
			}
		}
		return "";
	}

	public static boolean isMorning(String stime) {
		if (null != stime && "".equals(stime)) {
			Time time = new Time(stime);
			if (time.hour > 12) {
				return false;
			}
		}
		return true;
	}

	public static Bitmap compressImage(String picpath) {
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		// 开始读入图片，此时把options.inJustDecodeBounds 设回true了
		newOpts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(picpath, newOpts);// 此时返回bm为空

		newOpts.inJustDecodeBounds = false;
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;
		// 现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
		float hh = 600f;// 这里设置高度为800f
		float ww = 360f;// 这里设置宽度为480f
		// 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
		int be = 1;// be=1表示不缩放
		if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
			be = (int) (newOpts.outWidth / ww);
		} else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
			be = (int) (newOpts.outHeight / hh);
		}
		if (be <= 0)
			be = 1;
		newOpts.inSampleSize = be;// 设置缩放比例
		// 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
		bitmap = BitmapFactory.decodeFile(picpath, newOpts);
		return compressImage(bitmap);
	}

	public static Bitmap compressImage(Bitmap image) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 100, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
		int options = 100;
		while (baos.toByteArray().length / 1024 > 30) { // 循环判断如果压缩后图片是否大于100kb,大于继续压缩
			baos.reset();// 重置baos即清空baos
			image.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
			options -= 10;// 每次都减少10
		}
		ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
		Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);// 把ByteArrayInputStream数据生成图片
		return bitmap;
	}

}

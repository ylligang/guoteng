package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**  
 * @Project: GuoTeng
 * @Title: KeyCommendInfo.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO 元器件产品详情
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-21 上午11:07:05
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class KeyCommendInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String content;
}

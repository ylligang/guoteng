package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**  
 * @Project: GuoTeng
 * @Title: CommentKinds.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO  元器件分类
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-20 上午10:57:27
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class CommentKinds implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int category_id;
	public String title;
	
	
}

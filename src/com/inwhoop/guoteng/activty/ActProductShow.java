package com.inwhoop.guoteng.activty;

import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalBitmap;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.KeyCommendInfo;
import com.inwhoop.guoteng.model.ProductInfo;
import com.inwhoop.guoteng.util.JsonUtils;
import com.inwhoop.guoteng.util.Utils;
import com.inwhoop.guoteng.view.MyGridView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;

/**
 * @Project: GuoTeng
 * @Title: ProductShowActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO 产品展示
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-3-18 上午11:12:11
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ActProductShow extends BaseActivity {

	private MyGridView gridview = null;

	private List<ProductInfo> list = new ArrayList<ProductInfo>();

	private LayoutInflater inflater = null;

	private AlertDialog dialog = null;

	private int categoryid;

	public FinalBitmap fb;

	private KeyCommendInfo info = null;

	private PopupWindow popupWindow = null;
	
	private TextView nodata = null;
	
	private int count = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_product_layout);
		mContext = ActProductShow.this;
		inflater = LayoutInflater.from(mContext);
		categoryid = getIntent().getIntExtra("categoryid", 0);
		count = getIntent().getIntExtra("count", 0);
		fb = FinalBitmap.create(mContext);
		fb.configLoadingImage(R.drawable.default_local);
		fb.configLoadfailImage(R.drawable.default_local);
		findData();
	}

	private void findData() {
		gridview = (MyGridView) findViewById(R.id.progridview);
		nodata = (TextView) findViewById(R.id.nodata);
		read();
	}

	private void read() {
		if(count != 1){
			showProgressDialog("正在加载信息...");
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getProductList(categoryid);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				gridview.setVisibility(View.GONE);
				nodata.setVisibility(View.VISIBLE);
				break;

			case MyApplication.READ_SUCCESS:
				if(list.size()>0){
					gridview.setVisibility(View.VISIBLE);
					nodata.setVisibility(View.GONE);
					gridview.setAdapter(new MyAdapter());
					gridview.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							readCommendinfo(list.get(position).article_id);
						}
					});
				}else{
					gridview.setVisibility(View.GONE);
					nodata.setVisibility(View.VISIBLE);
				}
				break;

			default:
				break;
			}
		};
	};

	/**
	 * 读取元器件产品详情
	 * 
	 * @Title: readCommendinfo
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void readCommendinfo(final int id) {
		showProgressDialog("正在读取信息...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					info = JsonUtils.getKeyCommendInfo(id);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				infohandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler infohandler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("数据加载失败，请检查您的网络~~");
				break;

			case MyApplication.READ_SUCCESS:
//				View imgEntryView = inflater.inflate(
//						R.layout.product_info_layout, null); // 加载自定义的布局文件
//				dialog = new AlertDialog.Builder(mContext).create();
//				ImageView img = (ImageView) imgEntryView.findViewById(R.id.img);
//				WebView webView = (WebView) imgEntryView
//						.findViewById(R.id.infowebview);
//				TextView fail = (TextView) imgEntryView
//						.findViewById(R.id.failtext);
//				Utils.LoadWeb(webView, fail, info.content);
//				dialog.setView(imgEntryView); // 自定义dialog
//				dialog.show();
//				img.setOnClickListener(new OnClickListener() {
//					public void onClick(View paramView) {
//						dialog.cancel();
//					}
//				});
				showPopwindow();
				break;

			default:
				break;
			}
		};
	};

	/**
	 * 初始化快递下拉的popupwindow
	 */
	private void showPopwindow() {
		View view = LayoutInflater.from(this).inflate(
				R.layout.commend_info_popupwindow_layout, null);
		popupWindow = new PopupWindow(view, LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, true);
		popupWindow.setContentView(view);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(false);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		ImageView img = (ImageView) view.findViewById(R.id.img);
		img.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(null!=popupWindow){
					popupWindow.dismiss();
				}
			}
		});
		WebView webView = (WebView) view.findViewById(R.id.infowebview);
		TextView fail = (TextView) view.findViewById(R.id.failtext);
		Utils.LoadcontentWeb(webView, fail, info.content);
		popupWindow.showAtLocation(inflater.inflate(R.layout.beidou_terminal_layout, null), Gravity.CENTER, 0, 0);
	}

	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressWarnings("unused")
		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			Holder holder = null;
			if (null == holder) {
				holder = new Holder();
				convertView = inflater.inflate(
						R.layout.progridview_item_layout, null);
				holder.layout = (RelativeLayout) convertView
						.findViewById(R.id.itemrlea);
				holder.img = (ImageView) convertView.findViewById(R.id.img);
				LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
						android.widget.LinearLayout.LayoutParams.FILL_PARENT,
						MyApplication.sw / 3 - 15);
				holder.layout.setLayoutParams(parm);
				holder.version = (TextView) convertView
						.findViewById(R.id.pname);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			holder.version.setText(list.get(position).title);
			fb.display(holder.img, MyApplication.HOST
					+ list.get(position).img_url);
			return convertView;
		}

		class Holder {
			ImageView img;
			TextView version;
			RelativeLayout layout;
		}
	}

}

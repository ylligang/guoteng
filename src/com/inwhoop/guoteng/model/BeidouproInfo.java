package com.inwhoop.guoteng.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**  
 * @Project: GuoTeng
 * @Title: BeidouproInfo.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-18 下午2:50:28
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class BeidouproInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String content;
	public String description;
}

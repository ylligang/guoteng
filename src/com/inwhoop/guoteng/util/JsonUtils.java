package com.inwhoop.guoteng.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.graphics.YuvImage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.model.BarsetInfo;
import com.inwhoop.guoteng.model.BeidouproInfo;
import com.inwhoop.guoteng.model.CommenProInfo;
import com.inwhoop.guoteng.model.Commendinfo;
import com.inwhoop.guoteng.model.CommentKinds;
import com.inwhoop.guoteng.model.FavourActInfo;
import com.inwhoop.guoteng.model.Feedback;
import com.inwhoop.guoteng.model.KeyCommendInfo;
import com.inwhoop.guoteng.model.Picurl;
import com.inwhoop.guoteng.model.PrivateInfo;
import com.inwhoop.guoteng.model.ProductInfo;
import com.inwhoop.guoteng.model.SolutionInfo;
import com.inwhoop.guoteng.model.SolutionList;
import com.inwhoop.guoteng.model.NewsInfo;

public class JsonUtils {

	/**
	 * 读取企业概况
	 * 
	 * @Title: getQYKK
	 * @Description: TODO
	 * @param @return
	 * @return String
	 * @throws Exception
	 */
	public static String getQYKK() throws Exception {
		SyncHttp syncHttp = new SyncHttp();
		String result = syncHttp
				.httpGet(MyApplication.HOST, MyApplication.QYKK);
		return result;
	}

	/**
	 * 读取企业荣誉
	 * 
	 * @Title: getQYRY
	 * @Description: TODO
	 * @param @return
	 * @param @throws Exception
	 * @return String
	 */
	public static String getQYRY() throws Exception {
		SyncHttp syncHttp = new SyncHttp();
		String result = syncHttp
				.httpGet(MyApplication.HOST, MyApplication.QYRY);
		return result;
	}

	/**
	 * 读取运营服务
	 * 
	 * @Title: getQYRY
	 * @Description: TODO
	 * @param @return
	 * @param @throws Exception
	 * @return String
	 */
	public static String getYYFW() throws Exception {
		SyncHttp syncHttp = new SyncHttp();
		String result = syncHttp
				.httpGet(MyApplication.HOST, MyApplication.YYFW);
		return result;
	}

	/**
	 * 读取详情数据
	 * 
	 * @Title: getQYKK
	 * @Description: TODO
	 * @param url
	 *            接口地址
	 * @return String
	 * @throws Exception
	 */
	public static String getDetail(String url) throws Exception {
		SyncHttp syncHttp = new SyncHttp();
		String result = syncHttp.httpGet(url, "");
		return result;
	}

	/**
	 * 解析行业动态及公司新闻
	 * 
	 * @Title: parseIndustry
	 * @Description: TODO
	 * @param url
	 *            接口地址
	 * @return List<IndustryDynamic>
	 */
	public static List<NewsInfo> parseNews(String url) {
		List<NewsInfo> list = new ArrayList<NewsInfo>();
		try {
			SyncHttp syncHttp = new SyncHttp();
			String jsonStr = syncHttp.httpGet(MyApplication.HOST
					+ url, "");
			System.out.println("========jsonStr======"+jsonStr);
			JSONArray jsonArray = new JSONArray(jsonStr);
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				NewsInfo data = gson.fromJson(jsonObject.toString(),
						NewsInfo.class);
				list.add(data);
			}
			return list;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 获取资讯信息binner信息
	 * 
	 * @Title: getInfoBinner
	 * @Description: TODO
	 * @param @return
	 * @return List<NewsInfo>
	 * @throws Exception
	 */
	public static List<NewsInfo> getInfoBinner() throws Exception {
		List<NewsInfo> list = new ArrayList<NewsInfo>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.ADNEW, "");
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			NewsInfo data = gson
					.fromJson(jsonObject.toString(), NewsInfo.class);
			list.add(data);
		}
		return list;
	}

	/**
	 * 解析行业动态
	 * 
	 * @Title: parseIndustry
	 * @Description: TODO
	 * @param @return
	 * @return List<IndustryDynamic>
	 * @throws Exception
	 */
	public static List<FavourActInfo> getFavourActList() throws Exception {
		List<FavourActInfo> list = new ArrayList<FavourActInfo>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.YHHD, "");
		System.out.println("jsonStr= " + jsonStr);
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			System.out.println("jsonObject= " + jsonObject.toString());
			FavourActInfo data = gson.fromJson(jsonObject.toString(),
					FavourActInfo.class);
			list.add(data);
		}
		return list;
	}

	/**
	 * 解析解决方案列表
	 * 
	 * @Title: parseIndustry
	 * @Description: TODO
	 * @param @return
	 * @return List<IndustryDynamic>
	 * @throws Exception
	 */
	public static List<SolutionList> getSolutionlist() throws Exception {
		List<SolutionList> list = new ArrayList<SolutionList>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.JJFA, "");
		System.out.println("jsonStr= " + jsonStr);
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			System.out.println("jsonObject= " + jsonObject.toString());
			SolutionList data = gson.fromJson(jsonObject.toString(),
					SolutionList.class);
			list.add(data);
		}
		return list;
	}

	/**
	 * 私人定制上传信息接口
	 * 
	 * @Title: customer
	 * @Description: TODO
	 * @param @param companyName 公司名称
	 * @param @param name 姓名
	 * @param @param phone 电话
	 * @param @param msg 输入信息
	 * @param @return
	 * @return String 返回是否成功的信息
	 */
	public static String customer(PrivateInfo pinfo) {
		try {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			String json = gson.toJson(pinfo);
			String result = post(MyApplication.HOST + MyApplication.SRDZ,
					new String(json.getBytes("UTF-8"), "UTF-8"));
			JSONObject jsonObject = new JSONObject(result);
			String info = jsonObject.getString("return");
			if (info.equals("failed")) {
				return jsonObject.getString("msg");
			} else {
				return MyApplication.SUCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 栏目介绍
	 * 
	 * @Title: getBarsetlist
	 * @Description: TODO
	 * @param @return
	 * @param @throws Exception
	 * @return List<BarsetInfo>
	 */
	public static List<BarsetInfo> getBarsetlist() throws Exception {
		List<BarsetInfo> list = new ArrayList<BarsetInfo>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.BARSET, "");
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			BarsetInfo data = gson.fromJson(jsonObject.toString(),
					BarsetInfo.class);
			list.add(data);
		}
		return list;
	}

	/**
	 * 元器件分类信息
	 * 
	 * @Title: getCommentKinds
	 * @Description: TODO
	 * @param @return
	 * @param @throws Exception
	 * @return List<CommentKinds>
	 */
	public static List<CommentKinds> getCommentKinds() throws Exception {
		List<CommentKinds> list = new ArrayList<CommentKinds>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.YJQ, "");
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			CommentKinds data = gson.fromJson(jsonObject.toString(),
					CommentKinds.class);
			list.add(data);
		}
		return list;
	}

	/**
	 * 获取元器件产品列表
	 * 
	 * @Title: getProductList
	 * @Description: TODO
	 * @param @param categoryid
	 * @param @return
	 * @param @throws Exception
	 * @return List<ProductInfo>
	 */
	public static List<ProductInfo> getProductList(int categoryid)
			throws Exception {
		List<ProductInfo> list = new ArrayList<ProductInfo>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.COMMNETKINDS, "" + categoryid);
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			ProductInfo data = gson.fromJson(jsonObject.toString(),
					ProductInfo.class);
			list.add(data);
		}
		return list;
	}

	/**
	 * 北斗终端分类列表
	 * 
	 * @Title: getCommentKinds
	 * @Description: TODO
	 * @param @return
	 * @param @throws Exception
	 * @return List<CommentKinds>
	 */
	public static List<CommentKinds> getBDZDKinds() throws Exception {
		List<CommentKinds> list = new ArrayList<CommentKinds>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.BDZD, "");
		System.out.println("==========jsonStr==========" + jsonStr);
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			CommentKinds data = gson.fromJson(jsonObject.toString(),
					CommentKinds.class);
			list.add(data);
		}
		return list;
	}

	/**
	 * 获取元器件产品列表
	 * 
	 * @Title: getProductList
	 * @Description: TODO
	 * @param @param categoryid
	 * @param @return
	 * @param @throws Exception
	 * @return List<ProductInfo>
	 */
	public static List<ProductInfo> getBDZDProductList(int categoryid)
			throws Exception {
		List<ProductInfo> list = new ArrayList<ProductInfo>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.BDZDPRODUCTLIST, "" + categoryid);
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			ProductInfo data = gson.fromJson(jsonObject.toString(),
					ProductInfo.class);
			list.add(data);
		}
		return list;
	}

	/**
	 * 首页binner信息
	 * 
	 * @Title: getCommendinfolist
	 * @Description: TODO
	 * @param @return
	 * @param @throws Exception
	 * @return List<Commendinfo>
	 */
	public static List<Commendinfo> getCommendinfolist() throws Exception {
		List<Commendinfo> list = new ArrayList<Commendinfo>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.HOMEPAGECOMMENDLIST, "");
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			Commendinfo data = gson.fromJson(jsonObject.toString(),
					Commendinfo.class);
			list.add(data);
		}
		return list;
	}

	/**
	 * 联系我们
	 * 
	 * @Title: getContactInfo
	 * @Description: TODO
	 * @param @return
	 * @param @throws Exception
	 * @return ContactInfo
	 */
	public static ContactInfo getContactInfo() throws Exception {
		ContactInfo info = null;
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.CONTACTUS, "");
		JSONObject obj = new JSONObject(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		info = gson.fromJson(obj.toString(), ContactInfo.class);
		return info;
	}

	/**
	 * 常见问题
	 * 
	 * @Title: getCommenProInfolist
	 * @Description: TODO
	 * @param @return
	 * @param @throws Exception
	 * @return List<CommenProInfo>
	 */
	public static List<CommenProInfo> getCommenProInfolist() throws Exception {
		List<CommenProInfo> list = new ArrayList<CommenProInfo>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.COMMENPROBLEM, "");
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			CommenProInfo data = gson.fromJson(jsonObject.toString(),
					CommenProInfo.class);
			list.add(data);
		}
		return list;
	}

	/**
	 * 解决方案详情
	 * 
	 * @Title: getSolutionInfolist
	 * @Description: TODO
	 * @param @param id
	 * @param @return
	 * @param @throws Exception
	 * @return List<SolutionInfo>
	 */
	public static List<SolutionInfo> getSolutionInfolist(int id)
			throws Exception {
		List<SolutionInfo> list = new ArrayList<SolutionInfo>();
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.JJFAINFO, "" + id);
		JSONArray jsonArray = new JSONArray(jsonStr);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			SolutionInfo data = gson.fromJson(jsonObject.toString(),
					SolutionInfo.class);
			JSONArray array = jsonObject.getJSONArray("picurl");
			for (int j = 0; j < array.length(); j++) {
				JSONObject jObject = array.getJSONObject(j);
				Picurl pic =  gson.fromJson(jObject.toString(),
						Picurl.class);
				data.list.add(pic);
			}
			list.add(data);
		}
		return list;
	}

	/**
	 * 获取元器件产品详情
	 * 
	 * @Title: getKeyCommendInfo
	 * @Description: TODO
	 * @param @param id
	 * @param @return
	 * @param @throws Exception
	 * @return KeyCommendInfo
	 */
	public static KeyCommendInfo getKeyCommendInfo(int id) throws Exception {
		KeyCommendInfo info = null;
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.COMMNETINFO, "" + id);
		JSONArray array = new JSONArray(jsonStr);
		if (null != array && array.length() > 0) {
			JSONObject obj = array.getJSONObject(0);
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			info = gson.fromJson(obj.toString(), KeyCommendInfo.class);
		}
		return info;
	}

	/**
	 * 获取北斗终端产品详情
	 * 
	 * @Title: getBeidouproInfo
	 * @Description: TODO
	 * @param @param id
	 * @param @return
	 * @param @throws Exception
	 * @return BeidouproInfo
	 */
	public static BeidouproInfo getBeidouproInfo(int id) throws Exception {
		BeidouproInfo info = null;
		SyncHttp syncHttp = new SyncHttp();
		String jsonStr = syncHttp.httpGet(MyApplication.HOST
				+ MyApplication.BDZDPRODUCTINFO, "" + id);
		JSONArray array = new JSONArray(jsonStr);
		if (null != array && array.length() > 0) {
			JSONObject obj = array.getJSONObject(0);
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			info = gson.fromJson(obj.toString(), BeidouproInfo.class);
		}
		return info;
	}

	/**
	 * 留言板
	 * 
	 * @Title: feedback
	 * @Description: TODO
	 * @param @param companyName
	 * @param @param name
	 * @param @param phone
	 * @param @param msg
	 * @param @return
	 * @return String
	 */
	public static String feedback(Feedback feedback) {
		try {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			String json = gson.toJson(feedback);
			String result = post(MyApplication.HOST + MyApplication.FEEDBACK,
					new String(json.getBytes("UTF-8"), "UTF-8"));
			JSONObject jsonObject = new JSONObject(result);
			String info = jsonObject.getString("return");
			if (info.equals("failed")) {
				return jsonObject.getString("msg");
			} else {
				return MyApplication.SUCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 发送订单
	 * 
	 * @Title: post
	 * @Description: TODO
	 * @param @param http
	 * @param @param data
	 * @param @return
	 * @return String
	 */
	public static String post(String http, String data) {
		ByteArrayEntity arrayEntity = new ByteArrayEntity(data.getBytes());
		// arrayEntity.setContentType("binary/octet-stream");
		HttpPost httpPost = new HttpPost(http);
		httpPost.setEntity(arrayEntity);
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;
		try {
			response = client.execute(httpPost);
			int result = response.getStatusLine().getStatusCode();
			InputStream is = response.getEntity().getContent();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] bytedata = new byte[1024];
			int count = -1;
			while ((count = is.read(bytedata, 0, bytedata.length)) != -1) {
				outStream.write(bytedata, 0, count);
			}
			data = null;
			String s = new String(outStream.toByteArray(), "utf-8");
			return s;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

}

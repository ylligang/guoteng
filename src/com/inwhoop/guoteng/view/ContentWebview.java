package com.inwhoop.guoteng.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import android.webkit.WebView;

/**  
 * @Project: GuoTeng
 * @Title: ContentWebview.java
 * @Package com.inwhoop.guoteng.view
 * @Description: TODO 
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-25 下午6:05:20
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class ContentWebview extends WebView{

	public ContentWebview(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ContentWebview(Context context) {
		super(context);
	}

	public ContentWebview(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
	
}

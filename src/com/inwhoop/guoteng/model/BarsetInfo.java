package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**  
 * @Project: GuoTeng
 * @Title: BarsetInfo.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO 栏目介绍
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-20 上午10:24:50
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class BarsetInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int article_id;
	public String title;
	public String content;
	public String img_url;

}

package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**  
 * 公司新闻及行业动态
 * @Project: GuoTeng
 * @Title: NewsInfo.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO 新闻内容
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-14 上午9:29:54
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class NewsInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	 public int article_id;//文章id
	   public String title;
	   public String zhaiyao;
	   public String img_url;
}

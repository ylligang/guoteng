/**
 * 
 */
package com.inwhoop.guoteng.activty;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.Feedback;
import com.inwhoop.guoteng.util.JsonUtils;
import com.inwhoop.guoteng.util.Utils;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

/**
 * @author Administrator
 * 我要建议
 */
public class SuggestActivity extends BaseActivity implements OnClickListener{
	
	private EditText contentEditText;
	
	private Button submitButton = null;
	
	private String content = "";
	
	private InputMethodManager inputManager = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.we_suggest_layout);
		mContext = SuggestActivity.this;
		init();
	}
	
	@Override
	protected void init() {
		super.init();
		setTitleName("我要建议");
		setLeftButton(R.drawable.back);
		leftButton.setOnClickListener(this);
		contentEditText = (EditText) findViewById(R.id.fd_et);
		submitButton = (Button) findViewById(R.id.submit_btn);
		submitButton.setOnClickListener(this);
		inputManager = (InputMethodManager) contentEditText
				.getContext().getSystemService(
						Context.INPUT_METHOD_SERVICE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit_btn:
			submit();
			break;
		case R.id.left_bt:
			inputManager.hideSoftInputFromWindow(
					contentEditText.getWindowToken(), 0);
			finish();
			break;

		default:
			break;
		}
	}

	private void submit() {
		content = contentEditText.getText().toString().trim();
		if("".equals(content)){
			showToast("亲，您提问的内容不能为空~~");
			return;
		}
		showProgressDialog("正在提交...");
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Message msg = new Message();
				try {
					Feedback feedback = new Feedback();
					feedback.content = content;
					feedback.categoryId = 2;
					feedback.alias = Utils.getDEVICEID(mContext);
					String result = JsonUtils.feedback(feedback);
					if (result.equals(MyApplication.SUCESS)) {
						msg.what = MyApplication.READ_SUCCESS;
					} else {
						if ("".equals(result)) {
							msg.obj = "提交失败,请重试";
						} else {
							msg.obj = result;
						}
						msg.what = MyApplication.READ_FAIL;
					}
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("数据提交失败，请检查您的网络~~");
				break;

			case MyApplication.READ_SUCCESS:
				contentEditText.setText("");
				showToast("提交成功，感谢您对国腾电子的关注和支持");
				break;

			default:
				break;
			}
		};
	};
	
}

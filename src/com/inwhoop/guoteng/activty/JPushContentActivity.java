package com.inwhoop.guoteng.activty;

import cn.jpush.android.api.JPushInterface;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**  
 * @Project: GuoTeng
 * @Title: JPushContentActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-25 上午11:23:15
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class JPushContentActivity extends BaseActivity{
	
	private TextView contentTextView = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.movement_layout);
		MyApplication.sh = getWindowManager().getDefaultDisplay().getHeight();
		MyApplication.sw = getWindowManager().getDefaultDisplay().getWidth();
		init();
	}
	
	@Override
	protected void init() {
		super.init();
		setLeftButton(R.drawable.back);
		contentTextView = (TextView) findViewById(R.id.content);
		Intent intent = getIntent();

		if (null != intent) {
			Bundle bundle = getIntent().getExtras();
			String title = bundle
					.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE);
			setTitleName(""+title);
			String content = bundle.getString(JPushInterface.EXTRA_ALERT);
			contentTextView.setText(content);
		}
	}
	
}

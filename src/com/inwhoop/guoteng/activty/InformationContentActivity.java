/**
 * 
 */
package com.inwhoop.guoteng.activty;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.NewsInfo;
import com.inwhoop.guoteng.util.Utils;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * @author Administrator 资讯信息的二级页面
 */
public class InformationContentActivity extends BaseActivity implements
		OnClickListener {

	private final String ACT_ID = "actId";// 列表id的key

	private final String URL = "url";// 获取详细信息的接口地址

	private WebView webView = null;

	private TextView alertView, titTextView;

	private PopupWindow popupWindow = null;

	private String[] str = { "微信", "微信朋友圈", "QQ", "腾讯微博", "新浪微博" };

	private int[] imgs = { R.drawable.weixin, R.drawable.weixin,
			R.drawable.weixin, R.drawable.weixin, R.drawable.weixin };

	private LayoutInflater infalter = null;

	private NewsInfo info = null;

	private String url = "";

	// private String content = "";// 主要内容

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_info_layout);
		mContext = InformationContentActivity.this;
		infalter = LayoutInflater.from(mContext);
		init();
	}

	@Override
	protected void init() {
		super.init();
		setTitleName("资讯信息");
		setLeftButton(R.drawable.back);
		setRightButton(R.drawable.share_bg);
		setbackHome();
		backhomeImageView.setOnClickListener(this);
		rightButton.setOnClickListener(this);
		webView = (WebView) findViewById(R.id.contentwebview);
		alertView = (TextView) findViewById(R.id.alert_view);
		titTextView = (TextView) findViewById(R.id.title);
		// titTextView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//加粗
		// titTextView.getPaint().setFakeBoldText(true);//加粗
		Intent intent = getIntent();
		info = (NewsInfo) intent.getSerializableExtra("newsinfo");
		titTextView.setText(info.title);
		url = intent.getStringExtra(URL);
		getContent(info.article_id);
	}

	/**
	 * 根据列表id获取详情
	 * 
	 * @Title: getContent
	 * @Description: TODO
	 * @param @param id 传入的行业动态id
	 * @return void
	 */
	private void getContent(int id) {
		Utils.LoadWeb(webView, alertView, MyApplication.HOST + url
				+ info.article_id);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancel:
			if (popupWindow.isShowing()) {
				popupWindow.dismiss();
			}
			break;

		case R.id.right_bt:
			// showPopupwindow(v);
			Utils.showShare(InformationContentActivity.this, info.zhaiyao,
					MyApplication.HOST + info.img_url,MyApplication.HOST + url
					+ info.article_id);
			break;

		case R.id.two_bt:
			// showPopupwindow(v);
			finish();
			InformationActivity.backhome = true;
			break;

		default:
			break;
		}
	}

	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return str.length;
		}

		@Override
		public Object getItem(int position) {
			return str[position];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			if (null == convertView) {
				convertView = infalter.inflate(R.layout.share_gridview_item,
						null);
			}
			ImageView img = (ImageView) convertView.findViewById(R.id.img);
			img.setBackgroundResource(imgs[position]);
			TextView share = (TextView) convertView.findViewById(R.id.name);
			share.setText(str[position]);
			return convertView;
		}

	}

}

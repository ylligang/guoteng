package com.inwhoop.guoteng.adapter;

import java.util.List;

import net.tsz.afinal.FinalBitmap;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.NewsInfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @Project: GuoTeng
 * @Title: NewslistAdapter.java
 * @Package com.inwhoop.guoteng.adapter
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-3-14 上午9:32:13
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class NewslistAdapter extends BaseAdapter {

	private List<NewsInfo> list = null;

	private Context context = null;

	private LayoutInflater inflater = null;

	private FinalBitmap fb = null;

	public NewslistAdapter(Context context, List<NewsInfo> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
		fb = FinalBitmap.create(context);
		fb.configLoadfailImage(R.drawable.default_local);
		fb.configLoadingImage(R.drawable.default_local);
	}

	public void addList(List<NewsInfo> addlist) {
		for (int i = 0; i < addlist.size(); i++) {
			list.add(addlist.get(i));
		}
	}

	public List<NewsInfo> getList() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unused")
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if (null == holder) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.infonews_list_item, null);
			holder.img = (ImageView) convertView.findViewById(R.id.img);
			holder.title = (TextView) convertView.findViewById(R.id.title);
			holder.content = (TextView) convertView.findViewById(R.id.content);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		fb.display(holder.img, MyApplication.HOST + list.get(position).img_url);
		holder.title.setText(list.get(position).title);
		holder.content.setText(list.get(position).zhaiyao);
		return convertView;
	}

	class Holder {
		ImageView img;
		TextView title, content;
	}

}

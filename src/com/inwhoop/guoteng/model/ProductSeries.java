package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**  
 * @Project: GuoTeng
 * @Title: ProductSeries.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO 产品系列
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-14 上午11:51:20
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class ProductSeries implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public String title;
	public String introduction;

}

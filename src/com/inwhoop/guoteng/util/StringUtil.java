package com.inwhoop.guoteng.util;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**  
 * @Project: WZSchool
 * @Title: StringUtil.java
 * @Package com.wz.util
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-1-6 下午2:28:43
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class StringUtil {

	/**
	 * 
	 * @Title: isNull 
	 * @Description: 判断字符串是否为空
	 * @param @param str
	 * @param @return     
	 * @return boolean
	 */
	public static boolean isNull(String str) {
		boolean flag = false;
		if (null == str || str.trim().equals("")
				|| str.trim().equalsIgnoreCase("null")) {
			flag = true;
		}
		return flag;
	}
	
	/**
	 * 
	 * @Title: validateMoblie 
	 * @Description: TODO 验证电话号码
	 * @param @param phone
	 * @param @return     
	 * @return boolean
	 */
	public static boolean validateMoblie(String phone) {
		Pattern p = Pattern
				.compile("^((13[0-9])|(147)|(15[^4,\\D])|(18[0-9]))\\d{8}$");
		Matcher m = p.matcher(phone);
		return m.matches();
	}

	public static long getStrTime(String cc_time) {
		long l = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date d;
		try {
			d =  sdf.parse(cc_time);
			 l = d.getTime();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}
	
}

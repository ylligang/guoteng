/**
 * 
 */
package com.inwhoop.guoteng.activty;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.PrivateInfo;
import com.inwhoop.guoteng.util.JsonUtils;
import com.inwhoop.guoteng.util.StringUtil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

/**
 * @author Administrator 私人定制
 */
public class PrivateCustomActivity extends BaseActivity implements
		OnClickListener {

	private EditText nameEditText, phoneEditText, companyEditText,
			requestEditText;

	private String name, phone, address, request;

	private Button submitButton;
	
	private InputMethodManager inputManager = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
						| WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.private_layout);
		
		mContext = PrivateCustomActivity.this;
		init();
	}

	@Override
	protected void init() {
		super.init();
		setTitleName("私人定制");
		setLeftButton(R.drawable.back);
		leftButton.setOnClickListener(this);
		LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
				MyApplication.sw / 5 * 3, LayoutParams.WRAP_CONTENT);
		nameEditText = (EditText) findViewById(R.id.namedit);
		nameEditText.setLayoutParams(parm);
		phoneEditText = (EditText) findViewById(R.id.phonedit);
		phoneEditText.setLayoutParams(parm);
		companyEditText = (EditText) findViewById(R.id.companynamedit);
		companyEditText.setLayoutParams(parm);
		requestEditText = (EditText) findViewById(R.id.requestedit);
		submitButton = (Button) findViewById(R.id.submit_btn);
		submitButton.setOnClickListener(this);
		inputManager = (InputMethodManager) nameEditText
				.getContext().getSystemService(
						Context.INPUT_METHOD_SERVICE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit_btn:
			submit();
			break;
			
		case R.id.left_bt:
			inputManager.hideSoftInputFromWindow(
					nameEditText.getWindowToken(), 0);
			finish();
			break;

		default:
			break;
		}
	}

	private void submit() {
		name = nameEditText.getText().toString().trim();
		phone = phoneEditText.getText().toString().trim();
		address = companyEditText.getText().toString().trim();
		request = requestEditText.getText().toString().trim();
		String msg = check();
		if ("".equals(msg)) {
			showProgressDialog("正在提交，请稍后...");
			new Thread(new Runnable() {

				@Override
				public void run() {
					Message msg = new Message();
					try {
						PrivateInfo info = new PrivateInfo();
						info.CustomerName = address;
						info.UserName =  name;
						info.Tel = phone;
						info.Explain = request;
						String result = JsonUtils.customer(info);
						if (result.equals(MyApplication.SUCESS)) {
							msg.what = MyApplication.READ_SUCCESS;
						} else {
							if ("".equals(result)) {
								msg.obj = "提交失败,请重试";
							} else {
								msg.obj = result;
							}
							msg.what = MyApplication.READ_FAIL;
						}

					} catch (Exception e) {
						msg.what = MyApplication.READ_FAIL;
					}
					handler.sendMessage(msg);
				}
			}).start();
		} else {
			showToast(msg);
		}
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast((String) msg.obj);
				break;

			case MyApplication.READ_SUCCESS:
				showToast("提交成功，我们会尽快联系您，请保持通话畅通");
				nameEditText.setText("");
				phoneEditText.setText("");
				companyEditText.setText("");
				requestEditText.setText("");
				break;

			default:
				break;
			}
		};
	};

	/**
	 * 
	 * @Description:TODO验证
	 * @return
	 * @return String
	 * @author zhuw
	 * @date 2013-4-25 下午3:12:06
	 */
	private String check() {
		String msg = "";
		if (StringUtil.isNull(name)) {
			msg = "姓名不能为空";
		} else if (StringUtil.isNull(phone)) {
			msg = "联系方式不能为空";
		} else if (StringUtil.isNull(address)) {
			msg = "公司名称不能为空";
		} else if (StringUtil.isNull(request)) {
			msg = "需求不能为空";
		} else if (!StringUtil.validateMoblie(phone)) {
			msg = "电话号码格式不正确";
		}
		return msg;
	}

}

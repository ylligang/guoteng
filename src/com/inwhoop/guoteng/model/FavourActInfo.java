/**
 * 
 */
package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**
 * @author Administrator
 *
 */
public class FavourActInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String title;
	public String content;
	public String img_url;
	public String tel;
	
}

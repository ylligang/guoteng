package com.inwhoop.guoteng.activty;

import java.io.UnsupportedEncodingException;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.ProductInfo;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

/**  
 * @Project: GuoTeng
 * @Title: ProductInfoActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO 产品的详细信息
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-14 下午1:35:37
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class ProductInfoActivity extends BaseActivity implements OnClickListener{
	
	private ImageView deleteImageView = null;
	
	private WebView webView = null;
	
	private ProductInfo info = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_info_layout);
		mContext = ProductInfoActivity.this;
		findData();
	}

	private void findData() {
		deleteImageView = (ImageView) findViewById(R.id.img);
		deleteImageView.setOnClickListener(this);
		LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, MyApplication.sh/11*8);
		webView = (WebView) findViewById(R.id.infowebview);
		webView.setLayoutParams(parm);
		read();
	}
	
	private void read(){
		showProgressDialog("正在加载数据...");
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("数据加载失败，请检查您的网络~~");
				break;

			case MyApplication.READ_SUCCESS:
				try {
					webView.loadDataWithBaseURL(
							"fake://not/needed",
							"<html><head><meta http-equiv='content-type' content='text/html;charset=utf-8'><style type=\"text/css\">img{ width:95%}</style><STYLE TYPE=\"text/css\"> BODY { margin:0; padding: 5px 3px 5px 5px; background-color:#ececec;} </STYLE><BODY TOPMARGIN=5 rightMargin=0 MARGINWIDTH=0 MARGINHEIGHT=0></head><body>"
									+ new String("\t\t成都呐喊信息技术有限公司成立于2012年10月，公司致力于最专业的移动互联网解决方案。总部位于蓬勃发展" +
											"的移动互联网重镇成都市，团队核心成员均来自于前腾讯、百度、新浪等公司员工，他们在腾讯、百度、新浪工作期" +
											"间便参与了公司的移动互联网的相关重要项目，有着丰富的技术工作经验、产品体验意识总部位于蓬勃发展的移动互联网重镇成都市" +
											"，团队核心成员均来自于前腾讯、百度、新浪等公司员工，他们在腾讯、百度、新浪工作期间便参与了公司的移动互联网的相关重要项目，" +
											"有着丰富的技术工作经验、产品体验意识总部位于蓬勃发展的移动互联网重镇成都市，团队核心成员均来自于前腾讯、百度、新浪等公司员工，他" +
											"们在腾讯、百度、新浪工作期间便参与了公司的移动互联网的相关重要项目，有着丰富的技术工作经验、产品体验意识".getBytes("utf-8"))
									+ "</body></html>", "text/html", "utf-8", "");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				break;

			default:
				break;
			}
		};
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img:
			finish();
			break;

		default:
			break;
		}
	}
	
}

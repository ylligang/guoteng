/**
 * 
 */
package com.inwhoop.guoteng.activty;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.NewsInfo;
import com.inwhoop.guoteng.util.JsonUtils;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

/**
 * 资讯信息
 * @author Administrator
 * 
 */
public class InformationActivity extends BaseActivity implements OnClickListener{

	private ViewPager listPager;
	
	private ViewPager viewPager;

	private List<View> imgList = new ArrayList<View>();
	
	private List<View> contentList = new ArrayList<View>();

	private RelativeLayout imgLayout;

	private LayoutInflater inflater = null;

	private Button chatButton, gfButton;

	private View proline, connectline;
	
	private Class[] act = {ActInfomation.class,ActNews.class};
	
	private ImageView defaultImageView;

	private TextView titlenameTextView, nowpositionTextView;
	
	private List<NewsInfo> list = null;
	
	public static boolean backhome;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.information_layout);
		mContext = InformationActivity.this;
		inflater = LayoutInflater.from(mContext);
		backhome = false;
		init();
	}

	@Override
	protected void init() {
		super.init();
		setTitleName("资讯信息");
		setLeftButton(R.drawable.back);
		proline = findViewById(R.id.cwline);
		connectline = findViewById(R.id.connectline);
		imgLayout = (RelativeLayout) findViewById(R.id.img_rela);
		LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, (MyApplication.sh) / 3*1 - 15);
		imgLayout.setLayoutParams(parm);
		viewPager = (ViewPager) findViewById(R.id.showimg_pager);
		viewPager.setVisibility(View.GONE);
		defaultImageView = (ImageView) findViewById(R.id.default_img);
		titlenameTextView = (TextView) findViewById(R.id.titlename);
		titlenameTextView.setVisibility(View.GONE);
		nowpositionTextView = (TextView) findViewById(R.id.nowposition);
		nowpositionTextView.setVisibility(View.GONE);
		listPager = (ViewPager) findViewById(R.id.list_pager);
		chatButton = (Button) findViewById(R.id.cw_btn);
		chatButton.setOnClickListener(this);
		gfButton = (Button) findViewById(R.id.connect_btn);
		gfButton.setOnClickListener(this);
		read();
	}

	private void read() {
		showProgressDialog("正在加载信息");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getInfoBinner();
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("数据加载失败，请检查您的网络~~");
				break;

			case MyApplication.READ_SUCCESS:
				setViewPager();
				swithActivity();
				break;

			default:
				break;
			}
		};
	};

	/**
	 * activity跳转
	 * 
	 * @Title: swithActivity
	 * @Description: TODO
	 * @param @param position
	 * @return void
	 */
	public void swithActivity() {
		for (int i = 0; i < 2; i++) {
			Intent intent = new Intent(this, act[i]);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			String name = i + " subactivity";
			View subActivity = getLocalActivityManager().startActivity(name, intent).getDecorView();
			contentList.add(subActivity);
		}
		listPager.setAdapter(new ListPageAdapter());
		listPager.setOnPageChangeListener(new ListPagelistener());
	}

	/**
	 * 
	 * @Title: viewpagerInit
	 * @Description: TODO viewpager的初始化
	 * @param
	 * @return void
	 */
	private void setViewPager() {
		if (null != list && list.size() > 0) {
			viewPager.setVisibility(View.VISIBLE);
			titlenameTextView.setVisibility(View.VISIBLE);
			nowpositionTextView.setVisibility(View.VISIBLE);
			defaultImageView.setVisibility(View.GONE);
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, (MyApplication.sh) / 3*1 - 15);
			for (int i = 0; i < list.size(); i++) {
				View v = inflater.inflate(R.layout.gt_viewpager_item, null);
				v.setTag(i+1);
				ImageView img = (ImageView) v.findViewById(R.id.v_img);
				img.setLayoutParams(parm);
				img.setScaleType(ScaleType.CENTER_CROP);
				fb.display(img, MyApplication.HOST+list.get(i).img_url);
				imgList.add(v);
				v.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View view) {
						int p = (Integer) view.getTag();
						Intent intent = new Intent(mContext,
								InformationContentActivity.class);
						intent.putExtra("newsinfo", list.get(p-1));
						intent.putExtra("url", MyApplication.GSXW_DETAIL);
						startActivity(intent);
					}
				});
			}
			viewPager.setAdapter(new MyPageAdapter());
			viewPager.setOnPageChangeListener(new MyPagelistener());
			nowpositionTextView.setText("1/" + list.size());
			titlenameTextView.setText(list.get(0).title);
		}
	}

	class MyPagelistener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int position) {
			viewPager.setCurrentItem(position);
			titlenameTextView.setText(list.get(position).title);
			nowpositionTextView.setText((position+1)+"/"+list.size());
		}

	}

	class MyPageAdapter extends PagerAdapter {

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(imgList.get(arg1));
		}

		@Override
		public int getCount() {
			return imgList.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return (arg0 == arg1);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			((ViewPager) container).addView(imgList.get(position));
			return imgList.get(position);
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cw_btn:
			proline.setVisibility(View.VISIBLE);
			connectline.setVisibility(View.INVISIBLE);
			listPager.setCurrentItem(0);
			break;
		case R.id.connect_btn:
			proline.setVisibility(View.INVISIBLE);
			connectline.setVisibility(View.VISIBLE);
			listPager.setCurrentItem(1);
			break;

		default:
			break;
		}
	}
	
	
	class ListPagelistener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int position) {
			if (position == 0) {
				proline.setVisibility(View.VISIBLE);
				connectline.setVisibility(View.INVISIBLE);
			} else {
				proline.setVisibility(View.INVISIBLE);
				connectline.setVisibility(View.VISIBLE);
			}
		}

	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(backhome){
			finish();
		}
	}

	class ListPageAdapter extends PagerAdapter {

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(contentList.get(arg1));
		}

		@Override
		public int getCount() {
			return contentList.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return (arg0 == arg1);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			((ViewPager) container).addView(contentList.get(position));
			return contentList.get(position);
		}

	}

}

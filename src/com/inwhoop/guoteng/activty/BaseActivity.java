package com.inwhoop.guoteng.activty;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView.LayoutParams;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * activity基础框架
 * 
 * @Project: GuoTeng
 * @Title: BaseActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-3-10 上午2:39:49
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class BaseActivity extends Activity {
	public ImageButton leftButton = null;
	public ImageButton rightButton = null;
	public TextView titleView = null;
	public ProgressDialog progressDialog;// 加载进度框

	protected Context mContext = null;
	private RelativeLayout headLayout = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	/**
	 * 初始化
	 * 
	 * @Title: init
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	protected void init() {
		leftButton = (ImageButton) findViewById(R.id.left_bt);
		rightButton = (ImageButton) findViewById(R.id.right_bt);
		titleView = (TextView) findViewById(R.id.title_text);
		headLayout = (RelativeLayout) findViewById(R.id.head);
		RelativeLayout.LayoutParams parm = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, MyApplication.sh / 11 * 1);
		headLayout.setLayoutParams(parm);
	}

	/**
	 * 设置标题
	 * 
	 * @Title: setTitleName
	 * @Description: TODO
	 * @param @param resId
	 * @return void
	 */
	protected void setTitleName(int resId) {
		titleView.setText(resId);
	}

	/**
	 * 设置左边按钮图片
	 * 
	 * @Title: setLeftButton
	 * @Description: TODO
	 * @param @param resId
	 * @return void
	 */
	protected void setLeftButton(int resId) {
		leftButton.setBackgroundResource(resId);
	}

	/**
	 * 设置右边按钮图片
	 * 
	 * @Title: setRightButton
	 * @Description: TODO
	 * @param @param resId 图片id
	 * @return void
	 */
	protected void setRightButton(int resId) {
		rightButton.setVisibility(View.VISIBLE);
		rightButton.setBackgroundResource(resId);
	}

	/**
	 * 显示进度框
	 * 
	 * @Title: showProgressDialog
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showProgressDialog(String msg) {
		if (null == progressDialog) {
			progressDialog = new ProgressDialog(mContext);
		}
		progressDialog.setMessage(msg);
		progressDialog.setCancelable(true);
		progressDialog.show();
	}

	/**
	 * 关闭进度框
	 * 
	 * @Title: dismissProgressDialog
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	protected void dismissProgressDialog() {
		if (null != progressDialog && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	/**
	 * 显示toast
	 * 
	 * @Title: showToast
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showToast(String msg) {
		Toast.makeText(mContext, msg, 0).show();
	}
}

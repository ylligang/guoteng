/**
 * 
 */
package com.inwhoop.guoteng.activty;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.BarsetInfo;
import com.inwhoop.guoteng.model.CommentKinds;
import com.inwhoop.guoteng.model.SolutionList;
import com.inwhoop.guoteng.util.JsonUtils;
import com.inwhoop.guoteng.view.MyGridView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

/**
 * @author Administrator
 *解决方案
 *
 */
public class SolutionActivity extends BaseActivity{
	
	private MyGridView gridview = null;
	
	public List<SolutionList> list = new ArrayList<SolutionList>();
	
	private LayoutInflater inflater = null;
	
	private ScrollView scrollView = null;

	private BarsetInfo info = null;

	private TextView introTextView = null; // 简介

	private TextView failTextView = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.solution_layout);
		mContext = SolutionActivity.this;
		inflater = LayoutInflater.from(mContext);
		info = (BarsetInfo) getIntent().getSerializableExtra("info");
		init();
	}
	
	@Override
	protected void init() {
		super.init();
		setTitleName("解决方案");
		setLeftButton(R.drawable.back);
		gridview = (MyGridView) findViewById(R.id.progridview);
		scrollView = (ScrollView) findViewById(R.id.scroll);
		introTextView = (TextView) findViewById(R.id.introtext);
		failTextView = (TextView) findViewById(R.id.failtext);
		if (null != info)
			introTextView.setText(info.content);
		readPicinfo();
		
	}
	
	private void readPicinfo() {
		
		showProgressDialog("正在加载信息...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getSolutionlist();
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				failTextView.setVisibility(View.VISIBLE);
				scrollView.setVisibility(View.GONE);
				break;

			case MyApplication.READ_SUCCESS:
				gridview.setAdapter(new MyAdapter());
				gridview.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						Intent intent = new Intent(mContext, SolutionInfoActivity.class);
						Bundle bundle = new Bundle();
						bundle.putSerializable("info", list.get(position));
						intent.putExtras(bundle);
						startActivity(intent);
					}
				});
				break;

			default:
				break;
			}
		};
	};
	
	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressWarnings("unused")
		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			Holder holder = null;
			if (null == holder) {
				holder = new Holder();
				convertView = inflater.inflate(
						R.layout.solutiongridview_item_layout, null);
				holder.img = (ImageView) convertView.findViewById(R.id.img);
				RelativeLayout.LayoutParams parm = new RelativeLayout.LayoutParams(
						LayoutParams.FILL_PARENT, MyApplication.sw /3 );
				holder.img.setLayoutParams(parm);
				holder.version = (TextView) convertView
						.findViewById(R.id.pname);
				holder.layout = (LinearLayout) convertView.findViewById(R.id.textlayout);
				LinearLayout.LayoutParams parm1 = new LinearLayout.LayoutParams(
						android.widget.LinearLayout.LayoutParams.FILL_PARENT, MyApplication.sw / 6 );
				holder.layout.setLayoutParams(parm1);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			fb.display(holder.img, MyApplication.HOST+list.get(position).img_url);
			holder.version.setText(list.get(position).title);
			return convertView;
		}

		class Holder {
			ImageView img;
			TextView version;
			LinearLayout layout;
		}
	}
	
}

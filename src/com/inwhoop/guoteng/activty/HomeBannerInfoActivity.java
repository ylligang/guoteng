package com.inwhoop.guoteng.activty;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.Commendinfo;
import com.inwhoop.guoteng.util.Utils;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.TextView;

/**
 * @Project: GuoTeng
 * @Title: HomeBannerInfoActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO 首页banner详细信息
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-3-25 上午10:24:43
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class HomeBannerInfoActivity extends BaseActivity {

	private WebView webView = null;

	private TextView nodataTextView = null;

	private Commendinfo info = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_banner_info_layout);
		mContext = HomeBannerInfoActivity.this;
		info = (Commendinfo) getIntent().getSerializableExtra("info");
		init();
	}

	@Override
	protected void init() {
		super.init();
		setTitleName("广告详情");
		setLeftButton(R.drawable.back);
		setRightButton(R.drawable.share_bg);
		webView = (WebView) findViewById(R.id.webview);
		nodataTextView = (TextView) findViewById(R.id.nodata);
		Utils.LoadWeb(webView, nodataTextView, MyApplication.HOST
				+ MyApplication.HOMEPAGECOMMENDINFO + info.article_id);

		rightButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String shareInfo = info.zhaiyao;
				Utils.showShare(HomeBannerInfoActivity.this, shareInfo,
						MyApplication.HOST + info.img_url, MyApplication.HOST
								+ MyApplication.HOMEPAGECOMMENDINFO
								+ info.article_id);
			}
		});
	}

}

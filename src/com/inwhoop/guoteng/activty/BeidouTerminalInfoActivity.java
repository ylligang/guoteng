package com.inwhoop.guoteng.activty;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.BeidouproInfo;
import com.inwhoop.guoteng.model.ProductInfo;
import com.inwhoop.guoteng.util.JsonUtils;
import com.inwhoop.guoteng.util.Utils;
import com.inwhoop.guoteng.view.ContentWebview;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

/**  
 * @Project: GuoTeng
 * @Title: BeidouTerminalInfoActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO 北斗终端产品的详情
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-18 下午2:41:27
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class BeidouTerminalInfoActivity extends BaseActivity{
	
	private BeidouproInfo info = null;
	
	private ContentWebview webview = null;
	
	private LinearLayout functionLayout = null;
	
	private LayoutInflater inflater = null;
	
	private ProductInfo pinfo = null;
	
	private ScrollView scrollView = null;

	private TextView failTextView = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.beidou_terminal_info_layout);
		mContext = BeidouTerminalInfoActivity.this;
		inflater = LayoutInflater.from(mContext);
		pinfo = (ProductInfo) getIntent().getSerializableExtra("info");
		init();
	}
	
	@Override
	protected void init() {
		super.init();
		setTitleName("北斗终端");
		setLeftButton(R.drawable.back);
		webview = (ContentWebview) findViewById(R.id.prointro);
		functionLayout = (LinearLayout) findViewById(R.id.functionlayout);
		scrollView = (ScrollView) findViewById(R.id.scroll);
		scrollView.setVisibility(View.GONE);
		failTextView = (TextView) findViewById(R.id.failtext);
		readNet();
	}
	
	private void readNet() {
		showProgressDialog("正在加载信息...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					info = JsonUtils.getBeidouproInfo(pinfo.article_id);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				failTextView.setVisibility(View.VISIBLE);
				scrollView.setVisibility(View.GONE);
				break;

			case MyApplication.READ_SUCCESS:
				failTextView.setVisibility(View.GONE);
				scrollView.setVisibility(View.VISIBLE);
				setData();
				break;

			default:
				break;
			}
		};
	};
	
	private void setData(){
		Utils.LoadcontentWeb(webview, null, info.content);
		if("".equals(info.description)){
			functionLayout.setVisibility(View.GONE);
		}else{
			String[] str = info.description.split("#");
			for (int i = 0; i < str.length; i++) {
				if(!"".equals(str[i])){
					View view = inflater.inflate(R.layout.beidou_funtion_item, null);
					TextView text = (TextView) view.findViewById(R.id.funtioncontent);
					text.setText(str[i]);
					functionLayout.addView(view);
				}
			}
		}
	}
	
//	private BeidouproInfo getInfo(){
//		info = new BeidouproInfo();
//		info.intro = "\t\t我是速度发送到发送到发送到发送到发送到发送到发送到发送到发生地方阿斯顿发送到发送到发生地方";
//		for (int i = 0; i < 5; i++) {
//			info.list.add("水电费无法完全二房东法尔范人情恶染发全文");
//		}
//		return info;
//	}

	
}

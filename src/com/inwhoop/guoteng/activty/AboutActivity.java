package com.inwhoop.guoteng.activty;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.util.Utils;
import com.inwhoop.guoteng.view.MyWebView;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

/**
 * 走进国腾
 * 
 * @Project: GuoTeng
 * @Title: AboutActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-3-21 下午5:21:24
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */

public class AboutActivity extends BaseActivity implements OnClickListener {

	private MyWebView webView = null;

	private TextView alertView = null;

	private PopupWindow popupWindow = null;

	private List<TextView> textViews = new ArrayList<TextView>();

	private String[] titles = { "公司简介", "企业荣誉", "运营服务" };
	private String content = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_layout);
		mContext = AboutActivity.this;
		init();
	}

	@Override
	protected void init() {
		super.init();
		setTitleName("公司简介");
		setLeftButton(R.drawable.back);
		showPullimg();
		titleView.setOnClickListener(this);
		webView = (MyWebView) findViewById(R.id.webview);
		// webView.loadUrl(MyApplication.HOST+MyApplication.QYKK);
		alertView = (TextView) findViewById(R.id.alert_view);
		initPopwindow();
		// readContent(0);
		setContent(0);
	}

	private void showPopupwindow(View v) {
		popupWindow.showAsDropDown(v, 0, 10);
	}

	/**
	 * 初始化快递下拉的popupwindow
	 */
	private void initPopwindow() {
		View view = LayoutInflater.from(this).inflate(
				R.layout.about_popupwindow_layout, null);
		popupWindow = new PopupWindow(view, LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT, true);
		popupWindow.setContentView(view);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(false);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		TextView introTextView = (TextView) view
				.findViewById(R.id.companyintro);
		introTextView.setOnClickListener(this);
		TextView ryTextView = (TextView) view.findViewById(R.id.companyry);
		ryTextView.setOnClickListener(this);
		TextView serviceTextView = (TextView) view.findViewById(R.id.servicetv);
		serviceTextView.setOnClickListener(this);
		textViews.add(introTextView);
		textViews.add(ryTextView);
		textViews.add(serviceTextView);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.companyintro:
			setContent(0);
			break;

		case R.id.companyry:
			setContent(1);
			break;

		case R.id.servicetv:
			setContent(2);
			break;

		case R.id.title_text:
			showPopupwindow(v);
			break;

		default:
			break;
		}
	}

	private void setContent(int position) {
		setTitleName(titles[position]);
		for (int i = 0; i < textViews.size(); i++) {
			textViews.get(i).setTextColor(
					getResources().getColor(R.color.about_title_nochoice_bg));
		}
		textViews.get(position).setTextColor(
				getResources().getColor(R.color.about_title_choice_bg));
		popupWindow.dismiss();
		switch (position) {
		case 0:
			Utils.LoadWeb(webView, alertView, MyApplication.HOST
					+ MyApplication.QYKK);
			break;
		case 1:
			Utils.LoadWeb(webView, alertView, MyApplication.HOST
					+ MyApplication.QYRY);
			break;
		case 2:
			Utils.LoadWeb(webView, alertView, MyApplication.HOST
					+ MyApplication.YYFW);
			break;

		default:
			break;
		}
	}

}

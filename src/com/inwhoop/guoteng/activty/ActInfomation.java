/**
 * 
 */
package com.inwhoop.guoteng.activty;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.adapter.NewslistAdapter;
import com.inwhoop.guoteng.model.NewsInfo;
import com.inwhoop.guoteng.util.JsonUtils;
import com.inwhoop.guoteng.view.MyListView;
import com.inwhoop.guoteng.view.MyListView.OnRefreshListener;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 行业动态
 * 
 * @author Administrator
 * 
 */
@SuppressLint("HandlerLeak")
public class ActInfomation extends BaseActivity implements OnRefreshListener {

	private final String ACT_ID = "actId";// 列表id的key

	private final String URL = "url";// 获取详细信息的接口地址

	private MyListView listView = null;

	private List<NewsInfo> list = new ArrayList<NewsInfo>();

	private NewslistAdapter adapter = null;

	@SuppressWarnings("unused")
	private int page = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hynews_list_layout);
		mContext = ActInfomation.this;
		findview();
	}

	private void findview() {
		listView = (MyListView) findViewById(R.id.nlist);
		listView.setRefreshListener(this);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent = new Intent(mContext,
						InformationContentActivity.class);
				if (arg2 < adapter.getList().size()) {
					intent.putExtra("newsinfo", adapter.getList().get(arg2));
					intent.putExtra(URL, MyApplication.HYDT_DETAIL);
					startActivity(intent);
				}
			}
		});
		read();
	}

	private void read() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.parseNews(MyApplication.HYDT);
					if (null != list && list.size() > 0) {
						msg.what = MyApplication.READ_SUCCESS;
					} else {
						msg.what = MyApplication.READ_FAIL;
					}
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("数据加载失败，请检查您的网络~~");
				break;

			case MyApplication.READ_SUCCESS:
				if (list.size() < 10) {
					listView.isRefreshable = false;
				}
				page++;
				adapter = new NewslistAdapter(mContext, list);
				listView.setAdapter(adapter);
				break;

			default:
				break;
			}
		};
	};

	@Override
	public void onRefresh() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.parseNews(MyApplication.HYDT);
					if (null != list && list.size() > 0) {
						msg.what = MyApplication.READ_SUCCESS;
					} else {
						msg.what = MyApplication.READ_FAIL;
					}
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				addhandler.sendMessage(msg);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler addhandler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			listView.onRefreshComplete();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("数据加载失败，请检查您的网络~~");
				break;

			case MyApplication.READ_SUCCESS:
				if (list.size() < 10) {
					listView.isRefreshable = false;
				}
				page++;
				adapter.addList(list);
				adapter.notifyDataSetChanged();
				break;

			default:
				break;
			}
		};
	};

}

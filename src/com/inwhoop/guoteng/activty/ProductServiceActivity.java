package com.inwhoop.guoteng.activty;

import java.util.List;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.BarsetInfo;
import com.inwhoop.guoteng.util.JsonUtils;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

/**
 * @Project: GuoTeng
 * @Title: ProductServiceActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO 产品服务
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-3-14 上午10:22:30
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ProductServiceActivity extends BaseActivity {

	private LinearLayout allLayout;

	private Intent intent = null;

	private List<BarsetInfo> list = null;

	private TextView failTextView = null;

	private ScrollView scrollView = null;

	private LayoutInflater inflater = null;
	
	private int[] imgs = {R.drawable.product_one,R.drawable.product_two,R.drawable.product_three};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_service_layout);
		mContext = ProductServiceActivity.this;
		inflater = LayoutInflater.from(mContext);
		init();
	}

	@Override
	protected void init() {
		super.init();
		setTitleName("产品服务");
		setLeftButton(R.drawable.back);
		failTextView = (TextView) findViewById(R.id.failtext);
		scrollView = (ScrollView) findViewById(R.id.scroll);
		allLayout = (LinearLayout) findViewById(R.id.layout);
		read();
	}

	/**
	 * 读取栏目信息
	 * 
	 * @Title: read
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void read() {
		showProgressDialog("正在读取信息，请稍后...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getBarsetlist();
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				failTextView.setVisibility(View.VISIBLE);
				scrollView.setVisibility(View.GONE);
				break;

			case MyApplication.READ_SUCCESS:
				setData();
				break;

			default:
				break;
			}
		}

	};

	private void setData() {
		for (int i = 0; i < list.size(); i++) {
			View view = inflater.inflate(R.layout.barset_layout_item, null);
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, MyApplication.sh / 44 * 13 - 10);
			LinearLayout layout = (LinearLayout) view
					.findViewById(R.id.onelayout);
			layout.setLayoutParams(parm);
			TextView name = (TextView) view.findViewById(R.id.name1);
			name.setText(list.get(i).title);
			ImageView timg = (ImageView) view.findViewById(R.id.timg);
			timg.setBackgroundResource(imgs[i%imgs.length]);
			ImageView img = (ImageView) view.findViewById(R.id.img1);
			fb.display(img, MyApplication.HOST + list.get(i).img_url);
			allLayout.addView(view);
			view.setTag(i + 1);
			view.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					int position = (Integer) view.getTag();
					switch (position) {
					case 1:
						intent = new Intent(mContext,
								KeyComponentActivity.class);
						Bundle bundle = new Bundle();
						bundle.putSerializable("info", list.get(position-1));
						intent.putExtras(bundle);
						startActivity(intent);
						break;
					case 2:
						intent = new Intent(mContext,
								BeidouTerminalActivity.class);
						Bundle bundle1 = new Bundle();
						bundle1.putSerializable("info", list.get(position-1));
						intent.putExtras(bundle1);
						startActivity(intent);
						break;
					case 3:
						intent = new Intent(mContext, SolutionActivity.class);
						Bundle bundle2 = new Bundle();
						bundle2.putSerializable("info", list.get(position-1));
						intent.putExtras(bundle2);
						startActivity(intent);
						break;

					default:
						break;
					}
				}
			});
		}
	}

}

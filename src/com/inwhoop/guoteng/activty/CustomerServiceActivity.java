/**
 * 
 */
package com.inwhoop.guoteng.activty;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gouteng.app.MyApplication;
import com.inwhoop.guoteng.R;
import com.inwhoop.guoteng.model.CommenProInfo;
import com.inwhoop.guoteng.util.ContactInfo;
import com.inwhoop.guoteng.util.JsonUtils;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author Administrator 客户专区
 */
public class CustomerServiceActivity extends BaseActivity implements
		OnClickListener {

	private View proline, connectline;

	private ViewPager viewpager = null;

	private List<View> imgList = new ArrayList<View>();

	private LayoutInflater inflater = null;

	private List<CommenProInfo> list = null;

	private Button cwButton, connectButton;

	private Intent intent = null;

	private ContactInfo info = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.customer_service_layout);
		mContext = CustomerServiceActivity.this;
		inflater = LayoutInflater.from(mContext);
		init();
	}

	@Override
	protected void init() {
		super.init();
		setTitleName("客户专区");
		setLeftButton(R.drawable.back);
		proline = findViewById(R.id.cwline);
		connectline = findViewById(R.id.connectline);
		viewpager = (ViewPager) findViewById(R.id.showimg_pager);
		cwButton = (Button) findViewById(R.id.cw_btn);
		cwButton.setOnClickListener(this);
		connectButton = (Button) findViewById(R.id.connect_btn);
		connectButton.setOnClickListener(this);
		read();
	}

	private void read() {
		showProgressDialog("正在加载信息...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getCommenProInfolist();
					info = JsonUtils.getContactInfo();
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("数据加载失败~~");
				imgList.add(getCwView());
				imgList.add(getConnetView());
				setViewpager();
				break;

			case MyApplication.READ_SUCCESS:
				imgList.add(getCwView());
				imgList.add(getConnetView());
				setViewpager();
				break;

			default:
				break;
			}
		};
	};

	private void setViewpager() {
		viewpager.setAdapter(new MyPageAdapter());
		viewpager.setOnPageChangeListener(new MyPagelistener());
	}

	class MyPagelistener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int position) {
			if (position == 0) {
				proline.setVisibility(View.VISIBLE);
				connectline.setVisibility(View.INVISIBLE);
			} else {
				proline.setVisibility(View.INVISIBLE);
				connectline.setVisibility(View.VISIBLE);
			}
		}

	}

	class MyPageAdapter extends PagerAdapter {

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(imgList.get(arg1));
		}

		@Override
		public int getCount() {
			return imgList.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return (arg0 == arg1);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			((ViewPager) container).addView(imgList.get(position));
			return imgList.get(position);
		}

	}

	/**
	 * 得到常见问题view
	 * 
	 * @return
	 */
	private View getCwView() {
		View view = inflater.inflate(R.layout.commen_problem_layout, null);
		LinearLayout proLayout = (LinearLayout) view
				.findViewById(R.id.problemlayout);
		Button putButton = (Button) view.findViewById(R.id.problem_btn);
		putButton.setOnClickListener(this);
		Button suggestButton = (Button) view.findViewById(R.id.suggest_btn);
		suggestButton.setOnClickListener(this);
		if (null != list && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				View item = inflater
						.inflate(R.layout.problem_layout_item, null);
				TextView proTextView = (TextView) item
						.findViewById(R.id.problem_tv);
				proTextView.setText(list.get(i).title);
				final TextView answerTextView = (TextView) item
						.findViewById(R.id.answer_tv);
				answerTextView.setText(list.get(i).content);
				final ImageView img = (ImageView) item.findViewById(R.id.img);
				proTextView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (!answerTextView.isShown()) {
							img.setBackgroundResource(R.drawable.pulldown_bg);
							answerTextView.setVisibility(View.VISIBLE);
						} else {
							img.setBackgroundResource(R.drawable.pullright_bg);
							answerTextView.setVisibility(View.GONE);
						}
					}
				});
				proLayout.addView(item);
			}
		}
		return view;
	}

	/**
	 * 得到联系我们view
	 * 
	 * @return
	 */
	private View getConnetView() {
		View view = inflater.inflate(R.layout.connect_us_layout, null);
		Button putButton = (Button) view.findViewById(R.id.problem_btn);
		putButton.setOnClickListener(this);
		Button suggestButton = (Button) view.findViewById(R.id.suggest_btn);
		suggestButton.setOnClickListener(this);
		if (null != info) {
			TextView tel = (TextView) view.findViewById(R.id.tel);
			LinearLayout telLayout=(LinearLayout) view.findViewById(R.id.tel_layout);
			tel.setText(info.Webtel);
			telLayout.setOnClickListener(this);
			TextView saletel = (TextView) view.findViewById(R.id.saletel);
			LinearLayout saleLayout=(LinearLayout) view.findViewById(R.id.sale_layout);
			saletel.setText(info.WebSaleTel);
			saleLayout.setOnClickListener(this);
			TextView fax = (TextView) view.findViewById(R.id.fax);
			fax.setText(info.Webfax);
			TextView email = (TextView) view.findViewById(R.id.email);
			LinearLayout emailLayout=(LinearLayout) view.findViewById(R.id.email_layout);
			email.setText(info.Webmail);
			emailLayout.setOnClickListener(this);
			TextView qq = (TextView) view.findViewById(R.id.qq);
			qq.setText("QQ客服号码: " + info.WebQQ);
			TextView address = (TextView) view.findViewById(R.id.address);
			address.setText("公司地址: " + info.WebAddress);
			TextView code = (TextView) view.findViewById(R.id.code);
			code.setText("邮\t\t\t编: " + info.WebCode);
			TextView url = (TextView) view.findViewById(R.id.url);
			url.setText("公司官网: " + info.Weburl);
		}
		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cw_btn:
			proline.setVisibility(View.VISIBLE);
			connectline.setVisibility(View.INVISIBLE);
			viewpager.setCurrentItem(0);
			break;

		case R.id.connect_btn:
			proline.setVisibility(View.INVISIBLE);
			connectline.setVisibility(View.VISIBLE);
			viewpager.setCurrentItem(1);
			break;

		case R.id.problem_btn:
			intent = new Intent(mContext, PutAnswerActivity.class);
			startActivity(intent);
			break;

		case R.id.suggest_btn:
			intent = new Intent(mContext, SuggestActivity.class);
			startActivity(intent);
			break;

		case R.id.tel_layout:
			intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
					+ info.Webtel));
			startActivity(intent);
			break;

		case R.id.sale_layout:
			intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
					+ info.WebSaleTel));
			startActivity(intent);
			break;

		case R.id.email_layout:
			Intent email = new Intent(android.content.Intent.ACTION_SEND);
			email.setType("plain/text");
			String[] emailReciver = new String[] { info.Webmail };

			String emailSubject = "";
			String emailBody = "";
			// 设置邮件默认地址
			email.putExtra(android.content.Intent.EXTRA_EMAIL, emailReciver);
			// 设置邮件默认标题
			email.putExtra(android.content.Intent.EXTRA_SUBJECT, emailSubject);
			// 设置要默认发送的内容
			email.putExtra(android.content.Intent.EXTRA_TEXT, emailBody);
			// 调用系统的邮件系统
			startActivity(Intent.createChooser(email, "请选择邮件发送软件"));
			break;
		case R.id.url:
			Intent intent = new Intent();        
			intent.setAction("android.intent.action.VIEW");  
			Uri content_url = Uri.parse(info.Weburl);   
			intent.setData(content_url);  
			startActivity(intent);
			break;
		default:
			break;
		}
	}

}

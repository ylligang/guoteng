package com.inwhoop.guoteng.model;

import java.io.Serializable;

/**  
 * @Project: GuoTeng
 * @Title: ProductInfo.java
 * @Package com.inwhoop.guoteng.model
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-3-14 上午11:47:19
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class ProductInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int article_id;
	public String title;
	public String img_url;
}
